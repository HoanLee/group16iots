<script>
function createNode(element) {
return document.createElement(element);
}
function append(parent, el) {
return parent.appendChild(el);
}
const th1 = document.getElementById('studentId');
const th2 = document.getElementById('studentName');
const th3 = document.getElementById('date');
const url = 'https://studentattendmanagement.herokuapp.com/attendence';
fetch(url,{
method: 'GET',
})
.then(function(resp){
return resp.json()
})
.then(function(data) {
let authors = data.studentAttend;
return authors.map(function(author) {
let tr = createNode('tr'),
tr1 = createNode('tr'),
tr2 = createNode('tr'),
tr3 = createNode('tr'),
span = createNode('span'),
span1 = createNode('span'),
span2 = createNode('span'),
span3 = createNode('span');
span1.innerHTML = `${author.studentId}`;
span2.innerHTML = `${author.studentName}`;
span3.innerHTML = `${author.attendDate}`;

append(tr1, span1);
append(th1, tr1);
		  
append(tr2, span2);
append(th2, tr2);
		  
append(tr2, span2);
append(th2, tr2);
		  
append(tr3, span3);
append(th3, tr3);
			
})
}).catch(function(error) {
console.log(JSON.stringify(error));
});   
		  
 </script>